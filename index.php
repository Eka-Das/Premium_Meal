	<!doctype html>
	<html lang="es">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Bootstrap CSS -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<link rel="stylesheet" type="text/css" href="content/css/bootstrap.css">

		<style>

			body{
				background-color: black;
				color: white;
			}
			
			.btn-primary{
				background-color: black;
				color: yellow;
				border-color: yellow;
				
			}

			.btn-primary:hover{
				background-color: #1c1c1c;
				border-color: yellow;
				color: yellow;
			}

			.top-bar{
				background-color: black;
				color: yellow;
				border-bottom: 1px solid yellow ;
				
			}

		</style>

		<title>Panbazzini</title>
	</head>
	<body>
		<?php

		include('content/connection.php');

		$corte = "";
		$sql_numero_corte = "SELECT * FROM cortes ORDER BY id_corte ASC";

		$result_corte_actual = $conn->query($sql_numero_corte);

		if ($result_corte_actual->num_rows>0) {
			while ($row = $result_corte_actual->fetch_assoc()) {
				$corte = $row['id_corte'];
			}

		}else{
			$corte = "No hay corte actual";
			$corte2 = 0;
		}


		?>

		<div class="container-fluid">
			<!--
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<div class="container-fluid">
						<a class="navbar-brand" href="#">
							<img src="content/img/romanito-logo.jpg" alt="" width="30" height="24" class="d-inline-block align-text-top">
							Romanito's
						</a>

						<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
							<div class="navbar-nav">
								<a class="nav-link active" aria-current="page" href="#">Home</a>
								<a class="nav-link" href="#">Features</a>
								<a class="nav-link" href="#">Pricing</a>
								<a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
							</div>
						</div>
					</div>
				</nav>
			-->
			<div class="row top-bar">
				<div class="col-3">
				<img src="content/img/Panbazzini_Logo.png" class="img" alt="..." style="width: 80%;">
					
				</div>
				<div class="col-6">
				<?php
						date_default_timezone_set('America/Mexico_City');
						echo "Fecha: " . date("m-d") . "<br>";
						echo "Hora: " . date("H:i");

					?>
					
				</div>
				<div class="col-3">
					<?php

						echo "Corte #" . $corte;

					?>
				</div>
			</div>

			<?php
			


			if (isset($_GET['action'])) {

				$action = $_GET['action'];

				if ($action != "home") {
						# code...
				}

			}else{
				$action = "home";
			}

			switch ($action) {

				case 'home':

				include('content/home.php');

				break;

				case 'pedidos':
				include('content/pedidos/pizzas.php');
				break;

				case 'preparar_pedidos':
				include('content/pedidos/verPedidos.php');
				break;

				case 'pagar_pedidos':
				include('content/pedidos/pagar_pedidos.php');
				break;

				case 'pizza':
				include('content/pedidos/pizzas.php');
				break;

				case 'rollitos':
				include('content/pedidos/rollitos.php');
				break;

				case 'calzone':
				include('content/pedidos/calzone.php');
				break;

				case 'variable':
				include('content/pedidos/pedidos.php');
				break;

				case 'michaymicha':
				include('content/pedidos/michaymicha.php');
				break;

				case 'especial':
				include('content/pedidos/especial.php');
				break;

				case 'corte':
				include('content/corte/corte.php');
				break;

				case 'acumulado1':
					include('content/corte/acumulado1.php');

				break;

				case 'terminarDia':
				include('content/corte/terminarDia.php');
				break;

				case 'verCortes':
				include('content/corte/verCortes.php');
				break;

				case 'registrar_gasto':
				include('content/gastos/formulario_gasto.php');
				break;

				case 'verArchivado':
				include('content/corte/verArchivado.php');
				break;

				case 'inventario':
				include('content/inventario/inventario.php');
				break;

				case 'eliminarCorte':
					# code...
				break;


				default:
						# code...
				break;
			}

			?>
		</div>


		<!-- Optional JavaScript; choose one of the two! -->

		<!-- Option 1: Bootstrap Bundle with Popper -->
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
		<script type="text/javascript" src="content/js/bootstrap.js"></script>
		<!-- Option 2: Separate Popper and Bootstrap JS -->
		<!--
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
	-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script src="content/notify.js"></script>

	<script>
		$(function(){
			var URLsearch = window.location.search;
			//alert(URLsearch);
			if (URLsearch == "?action=pedidos&estado=success") {
				$.notify("REGISTRADO", "success");	
			}
		});

		$(function(){
			var URLsearch = window.location.search;
			//alert(URLsearch);
			if (URLsearch == "?action=pagar_pedidos&estado=pagado") {
				$.notify("Pagado", "success");	
			}
		});

		$(function(){
			var URLsearch = window.location.search;
			//alert(URLsearch);
			if (URLsearch == "?action=terminarDia&estado=success") {
				$.notify("CORTE CREADO", "success");	
			}
		});

		$(function(){
			var URLsearch = window.location.search;
			//alert(URLsearch);
			if (URLsearch == "?action=registrar_gasto&estado=success") {
				$.notify("Gasto Registrado", "success");	
			}
		});

		$(function(){
			var URLsearch = window.location.search;
			//alert(URLsearch);
			if (URLsearch == "?action=inventario&estado=success") {
				$.notify("Listo", "success");	
			}
		});
		
		$(function(){
			var URLsearch = window.location.search;
			//alert(URLsearch);
			if (URLsearch == "?action=terminarDia&eliminado=success") {
				$.notify("Eliminado", "success");	
			}
		});
		
	</script>


	<script>
		$(function(){
			$('.listo').click(function(){
				var id = $(this).attr('value');

				console.log(id);

				$.ajax({
					type:'POST',
					url:'content/pedidos/editar_estado_preparar.php',
					data : {'id':id}
				}).done(function(r){
					console.log(r);
					location.reload();
				});
			})
		});

		$(function(){
			$('.pagar').click(function(){
				var id = $(this).attr('value');

				console.log(id);

				$.ajax({
					type:'POST',
					url:'content/pedidos/editar_estado_pago.php',
					data : {'id':id}
				}).done(function(r){
					console.log(r);
					location.href="?action=pagar_pedidos&estado=pagado";
				});
			})
		});

		$(function(){
			$('.conseguido').click(function(){
				var id = $(this).attr('value');

				console.log(id);

				$.ajax({
					type:'POST',
					url:'content/inventario/editarEstadoInventario.php',
					data : {'id':id}
				}).done(function(r){
					console.log(r);
					location.href="?action=inventario&estado=success";
				});
			})
		});




			/*
				$.ajax({
					url: "productosdata.php?id="+id,
				}).done(function(res) {
					console.log(res)
					res = JSON.parse(res);
					$("#editId").val(res.id)
					$("#editName").val(res.product_name)
					$("#editEmail").val(res.precio)
					$("#editPass").val(res.descripcion)
				});
				*/

				var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
				var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
					return new bootstrap.Popover(popoverTriggerEl)
				})

			</script>
			
			<script type="text/javascript" src="content/datepicker/js/locales/bootstrap-datepicker.es.js" charset="UTF-8"></script>
		</body>
		</html>
