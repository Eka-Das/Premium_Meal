<a href="?action=home" class="btn btn-primary">Regresar</a>
<h1 class="text-center">Inventario</h1>
<form action="content/inventario/insertarArticulo.php" method="POST">

	<input class="form-control form-control-lg " type="text" placeholder="Artículo" aria-label=".form-control-lg example" name="articulo">
	<br>
	<button type="submit" class="btn btn-primary">Registrar</button>
</form>
<?php

//include('../connection.php');

$sql_ver_inventario = "SELECT * FROM inventario";

$result = $conn->query($sql_ver_inventario);


if ($result->num_rows > 0) {?>

	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th scope="col">Artículo</th>
					<th scope="col">Estado</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$pedidos_pendientes = 0;

				while($row = $result->fetch_assoc()) {
					if ($row['estado_inventario'] == "PENDIENTE") {?>
						<tr>
							<th scope="col"><?php echo $row['nombre_articulo']; ?></th>
							<th scope="col">
								<button type="button" class="conseguido btn btn-success" 
								value="<?php echo $row["id_inventario"]; ?>" >
									Listo
								</button>
							</th>
						</tr>
					<?php

				} 
			}

			?>
		</tbody>
	</table>
</div>
<?php

}
?>


