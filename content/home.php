  <div class="row justify-content-md-center pt-3">
    <div class="col-12 text-center order-first">
      <div class="d-grid gap-2 col-6 mx-auto">
        <a class="btn btn-primary" type="button" href="?action=pedidos">PEDIR</a>
        <a class="btn btn-primary" type="button" href="?action=pagar_pedidos">PAGAR</a>
        <a class="btn btn-primary" type="button" href="?action=preparar_pedidos">PREPARAR</a>

        <hr>
        <a class="btn btn-primary" type="button" href="?action=corte">Corte</a>
        <a class="btn btn-primary" type="button" href="?action=inventario">Inventario</a>
        
      </div>
    </div>
  </div>
</div>