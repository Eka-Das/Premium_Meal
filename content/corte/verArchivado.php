<a type="button" class="btn btn-primary btn-sm" href="?action=verCortes">Ver Cortes</a>
<?php

include('content/connection.php');
if ($_GET) {
	$id_corte = $_GET['id_corte'];
}
$sql_ver_pedidos = "SELECT * FROM pedidos WHERE id_corte_asociado = '$id_corte'";

$result = $conn->query($sql_ver_pedidos);


if ($result->num_rows > 0) {?>

	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Pedido</th>
					<th scope="col">Precio</th>
					<!--<th scope="col">Hora</th>-->
				</tr>
			</thead>
			<tbody>
				<?php
				$pedidos_pendientes = 0;
				$total=0;
				$contador = 1;
				while($row = $result->fetch_assoc()) {



					echo 
					"<tr>".
						"<th>" . $contador ."</th>". 
						"<th>" . $row["pedido"] ."</th>". 
						"<th>" . $row["precio"]. "</th>". 
					"</tr>";
					$pedidos_pendientes = +1;

					$total += $row["precio"];
					$contador++;
				}

				$sql_ver_gastos = "SELECT * FROM gastos WHERE id_asociado_corte_gasto = '$id_corte'";
				$result_ver_gastos = $conn->query($sql_ver_gastos);
				$total_gastos = 0;

				if ($result_ver_gastos->num_rows > 0) {
					while ( $row_gastos = $result_ver_gastos->fetch_assoc()) {
						?>
						<tr class="table-danger">
							<!--<th scope="row"><?php echo $cantidad_pedidos; ?></th>-->
							<td><?php echo $contador; ?></td>
							<td><?php echo $row_gastos['concepto_gasto']; ?></td>
							<td>-$<?php echo $row_gastos['cantidad_gasto']; ?> </td>
						</tr>

						<?php
						$total_gastos += $row_gastos['cantidad_gasto'];
						$contador++;
						//$cantidad_pedidos += 1;
					}


				}else{
					echo "no hay gastos registrados";
				}



				?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2">Venta Total: </td>
					<td ><b><?php echo "$" . $total ?></b></td>
				</tr>
				<tr>
					<td colspan="2">Total Gastos: </td>
					<td ><b><?php echo "$" . $total_gastos; ?></b></td>
				</tr>
				<tr>
					<td colspan="2">Residual: </td>
					<td ><b><?php echo "$" . $total - $total_gastos; ?></b></td>
				</tr>
			</tfoot>
		</table>
	</div>

	<?php
}
if(isset($pedidos_pendientes) == 0){
	echo "No hay pedidos pendientes";
}
?>
