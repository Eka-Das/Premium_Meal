	
<div class="row justify-content-md-center">
	<div class="col-12 order-first">
		<a type="button" class="btn btn-primary btn-sm" href="?action=corte">Regresar</a>
		<div class="text-center">
			<h1>
				<b>Fin De Día</b>
				
				<span class=" material-icons d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="Aquí se muestra el registro de la venta del día y los gastos generados">
					info
				</span>
			</h1>


			<div class="row justify-content-md-center">
				<div class="col-12 order-first">

					<?php

					include('content/connection.php');

					$sql_ver_pedidos = "SELECT * FROM pedidos WHERE id_corte_asociado = $corte";

					$result_ver_pedidos = $conn->query($sql_ver_pedidos);


					if ($result_ver_pedidos->num_rows > 0) {?>

						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">Concepto</th>
										<th scope="col">Cantidad</th>
										<th scope="col">X</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$pedidos_pendientes = 0;
									$cantidad_pedidos = 0;
									$total = 0;

									while($row = $result_ver_pedidos->fetch_assoc()) {
										if ($row['estado_pago_pedido'] == "PAGADO") {?>

											<tr class="table-success">
												<th scope="row"><?php echo $cantidad_pedidos; ?></th>
												<td><?php echo $row["pedido"]; ?></td>
												<td><?php echo "$" . $row["precio"]; ?></td>
												<td>
													<a href="content/corte/eliminarPedido.php?&id=<?php echo $row['id_pedido']; ?>">
														<b>
															X
														</b>
													</a>
												</td>
											</tr>

											<?php
											$pedidos_pendientes = +1;

										}


										if ($row['estado_pago_pedido'] !== "PAGADO") {?>

											<tr class="table-danger">
												<th scope="row"><?php echo $cantidad_pedidos; ?></th>
												<td><?php echo $row["pedido"]; ?></td>
												<td><?php echo "$" . $row["precio"]; ?></td>
												<td>
													<a href="content/corte/eliminarPedido.php?&id=<?php echo $row['id_pedido']; ?>">
														<b>
															X
														</b>
													</a>
												</td>
											</tr>

											<?php

											$pedidos_pendientes = +1;

										}

										$cantidad_pedidos +=1;
										$total += $row['precio']; 
									}

									$sql_ver_gastos = "SELECT * FROM gastos WHERE id_asociado_corte_gasto = '$corte'";
									$result_ver_gastos = $conn->query($sql_ver_gastos);
									$total_gastos = 0;

									if ($result_ver_gastos->num_rows > 0) {
										while ( $row_gastos = $result_ver_gastos->fetch_assoc()) {
											?>
											<tr class="table-danger">
												<th scope="row"><?php echo $cantidad_pedidos; ?></th>
												<td><?php echo $row_gastos['concepto_gasto']; ?></td>
												<td>-$<?php echo $row_gastos['cantidad_gasto']; ?> </td>

											</tr>

											<?php
											$total_gastos += $row_gastos['cantidad_gasto'];
											$cantidad_pedidos += 1;
										}


									}else{
										echo "no hay gastos registrados";
									}


									?>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2">Venta Total: </td>
										<td ><b><?php echo "$" . $total ?></b></td>
									</tr>
									<tr>
										<td colspan="2">Total Gastos: </td>
										<td ><b><?php echo "$" . $total_gastos; ?></b></td>
									</tr>
									<tr>
										<td colspan="2">Residual Total: </td>
										<td ><b><?php echo "$" . $total - $total_gastos; ?></b></td>
									</tr>
								</tfoot>
							</table>
						</div>
						<div class="d-grid gap-2 col-6 mx-auto">
							<a class="mx-auto btn btn-primary" type="button" href="content/corte/cerrarDia.php?corte=<?php echo $corte ?>">Cerrar Día</a>
							<span class="p-3 float-left material-icons d-inline-block" tabindex="0" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-content="El botón de 'cerrar día' pone en 0 el registro y crea un nuevo día sin registros">
								info
							</span>

							<!--<a class="btn btn-primary" type="button" href="verCortes.php">Ver cortes</a>-->
						</div>

						<?php
					}else{
						echo "<br>No hay pedidos<br>";
						$pedidos_pendientes = 0;
					}

					if($pedidos_pendientes == 0){
						echo "No hay pedidos pendientes";
					}

					if (isset($corte2) && $corte2 == 0 ) {
						?>
						<a class="mx-auto btn btn-primary" type="button" href="content/corte/insertar_nuevo_corte.php">Crear nuevo corte</a>
						<?php
					}

					?>					


				</div>
			</div>

		</div>
	</div>
</div>
