<a type="button" class="btn btn-primary btn-sm" href="?action=home">Home</a>
<?php
	
	include('content/connection.php');
	
	$sql_ver_pedidos = "SELECT * FROM pedidos";

	$result = $conn->query($sql_ver_pedidos);


	if ($result->num_rows > 0) {?>

		<div class="table-responsive">
			<table class="table">
			  <thead>
			    <tr>
			      <th scope="col">Pedido</th>
			      <th scope="col">Precio</th>
			      <th scope="col">Mesa</th>
			      <th scope="col">Estado</th>
			    </tr>
			  </thead>
			  <tbody>
			    <?php
			    	$pedidos_pendientes = 0;

					while($row = $result->fetch_assoc()) {
						if ($row['estado_pedido'] == "PENDIENTE") {
							
						
							echo 
								"<tr>".
									"<th>" . $row["pedido"] ."</th>". 
									"<th>$" . $row["precio"]. "</th>". 
									//"<th>" . $row["fecha_pedido"]. "</th>". 
									//"<th>". $row['estado_pedido'] ."</th>". 
									"<th>" . $row["mesa"] . "</th>".
									'<th> <button type="button" class="listo btn btn-success" value="'.$row["id_pedido"].'" >Listo</button> </th>'.
								"</tr>";
								$pedidos_pendientes = +1;
							
						}
					}
			    ?>
			  </tbody>
			</table>
		</div>

<?php
	}
	if($pedidos_pendientes == 0){
		echo "No hay pedidos pendientes";
	}
?>
