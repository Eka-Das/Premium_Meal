<div class="pt-3">
	<a type="button" class="btn btn-primary btn-sm" href="?action=home">Menú</a>
</div>
<div class="row justify-content-md-center text-center ">


	<div class="col-12 order-first pt-3">
		
		<center> <h1 class="text-center">Realizar pedido</h1> </center>

		<div class=" btn-group-vertical">


			<div class="btn-toolbar mb-3" role="toolbar" aria-label="Toolbar with button groups">
				<form action="content/pedidos/hacerPedido.php" method="POST">
					<div class="mb-3">
						<label for="exampleDataList" class="form-label">Pedido</label>
						<input class="form-control" list="datalistOptions" id="exampleDataList" placeholder="Pedido" required="" name="pedido">
						<datalist id="datalistOptions">
							<option value="Pepperoni"></option>
							<option value="Hawaiana"></option>
							<option value="Bolognesa"></option>
							<option value="Camarón"></option>
							<option value="Chorizo"></option>
							<option value="Rollitos"></option>
							<option value="Calzone"></option>
						</datalist>

						<label for="exampleInputPassword1" class="form-label">Mesa</label>
						<input type="number" class="form-control" id="exampleInputPassword1" name="mesa" required="">

						<label for="exampleInputPassword1" class="form-label">Precio</label>
						<input type="number" class="form-control" id="exampleInputPassword1" name="precio" required="">
					</div>

					<div class="form-check pb-3">
						<input class="form-check-input" type="checkbox" value="PAGADO" name="estado_pedido_pago" id="flexCheckDefault">
						<label class="form-check-label" for="flexCheckDefault">
							Pagado
						</label>
						<input type="hidden" name="id_corte" value="<?php echo $corte ?>">
					</div>
					<button type="submit" class="btn btn-primary">Realizar pedido</button>
				</form>		

				<!--
					<div class="input-group mb-3">
						
						<button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">Pepperoni</button>
						
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Chica</a></li>
							<li><a class="dropdown-item" href="#">Grande</a></li>
						</ul>
						
						
						<button class="btn btn-danger" type="button">-</button>
						<button class="btn btn-success" type="button">+</button>
						
						<input type="number" class="form-control" placeholder="" aria-label="Example text with two button addons">       
					</div>

					<div class="input-group mb-3">
						
						<button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">Hawaiana</button>
						
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Chica</a></li>
							<li><a class="dropdown-item" href="#">Grande</a></li>
						</ul>
						
						
						<button class="btn btn-danger" type="button">-</button>
						<button class="btn btn-success" type="button">+</button>
						
						<input type="number" class="form-control" placeholder="" aria-label="Example text with two button addons">      
					</div>

					<div class="input-group mb-3">
						
						<button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">Bolognesa</button>
						
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Chica</a></li>
							<li><a class="dropdown-item" href="#">Grande</a></li>
						</ul>
						
						
						<button class="btn btn-danger" type="button">-</button>
						<button class="btn btn-success" type="button">+</button>
						
						<input type="number" class="form-control" placeholder="" aria-label="Example text with two button addons">
					</div>

					<div class="input-group mb-3">
						
						<button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">Chorizo</button>
						
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Chica</a></li>
							<li><a class="dropdown-item" href="#">Grande</a></li>
						</ul>
						
						
						<button class="btn btn-danger" type="button">-</button>
						<button class="btn btn-success" type="button">+</button>
						
						<input type="number" class="form-control" placeholder="" aria-label="Example text with two button addons">        
					</div>

					<div class="input-group mb-3">
						
						<button class="btn btn-primary dropdown-toggle select-pizza" type="button" data-bs-toggle="dropdown" 
						aria-expanded="false">Margarita</button>
						
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Chica</a></li>
							<li><a class="dropdown-item" href="#">Grande</a></li>
						</ul>
						
						
						<button class="btn btn-danger" type="button">-</button>
						<button class="btn btn-success" type="button">+</button>
						
						<input type="number" class="form-control" placeholder="" 
						aria-label="Example text with two button addons">
					</div>

					<div class="input-group mb-3">
						
						<button class="btn btn-primary dropdown-toggle select-pizza" type="button" data-bs-toggle="dropdown" aria-expanded="false">Camarón</button>
						
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Chica</a></li>
							<li><a class="dropdown-item" href="#">Grande</a></li>
						</ul>
						
						
						<button class="btn btn-danger" type="button">-</button>
						<button class="btn btn-success" type="button">+</button>
						
						<input type="number" class="form-control" placeholder="" aria-label="Example text with two button addons">       
					</div>
				-->
			</div>
			
			<hr>
			<!--
					<a type="button" class="btn btn-primary btn-sm" href="?action=michaymicha">Micha & Micha</a>
					<a type="button" class="btn btn-primary btn-sm" href="?action=especial">Especial</a>
				-->
			</div>
		</div>
	</div>
