
<div class="row justify-content-md-center text-center ">


  <div class="col-12 order-first ">

    <div class=" btn-group-vertical">

      <a type="button" class="btn btn-primary btn-sm text-center padding-top-3" href="?action=home">home</a>
      
      <h1 class="text-center">Registrar Gasto</h1>

      <div class="btn-toolbar mb-3" role="toolbar" aria-label="Toolbar with button groups">
        <form action="content/gastos/insertar_gasto.php" method="POST">
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Concepto</label>
            <input type="text" class="form-control" id="exampleInputEmail1" 
            aria-describedby="emailHelp" name="concepto_gasto" required="">

            <label for="exampleInputPassword1" class="form-label">Monto</label>
            <input type="number" class="form-control" id="exampleInputPassword1" name="monto_gasto" required="">
            <input type="hidden" name="id_gasto_asociado" value="<?php echo $corte; ?>">
          </div>
          <button type="submit" class="btn btn-primary">Registrar</button>
        </form>

      </div>
      
      <hr>
      <!--
          <a type="button" class="btn btn-primary btn-sm" href="?action=michaymicha">Micha & Micha</a>
          <a type="button" class="btn btn-primary btn-sm" href="?action=especial">Especial</a>
        -->
      </div>
    </div>
  </div>
